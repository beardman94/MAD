import urllib.request, json 
import pandas as pd
with urllib.request.urlopen("http://mapa.um.warszawa.pl/WebServices/Ulice/wgs84/findAll/") as url:
    data = json.loads(url.read().decode())
    


"""
Latitude = 52
Long = 21
"""

res_df = pd.DataFrame(columns=['Long','Lat','Street_id','Point_id','Dzielnice', 'Nazwa_skrocona', 'Nazwa_peln'])

#data['features'][1]['geometry']['coordinates'][0][0][0]

"""

dzielnica
lokalizacja
typ_nawierzchni
typ_trasy

"""

street_id = 1

for feature in data['features']:
    dzielnica = feature['properties']['dzielnice']
    nazwa_skrocona = feature['properties']['nazwa_skrocona']
    nazwa_peln = feature['properties']['nazwa_peln']
    #typ_trasy = feature['properties']['typ_trasy']
    if feature['geometry']['type'] == 'LineString':
        point_id = 1
        for point in feature['geometry']['coordinates']:
            #print(point)
            res_df = res_df.append({'Long' : point[0], 
                                   'Lat': point[1], 
                                   'Street_id':street_id,
                                   'Point_id':point_id,
                                   'Dzielnice' : dzielnica, 
                                   'Nazwa_skrocona' : nazwa_skrocona, 
                                   'Nazwa_peln': nazwa_peln}, ignore_index = True)
            point_id += 1
        street_id += 1
    elif feature['geometry']['type'] != 'MultiPolygon':
        for street in feature['geometry']['coordinates']:
            point_id = 1
            for point in street:
                #print(point)
                res_df = res_df.append({'Long' : point[0], 
                                   'Lat': point[1], 
                                   'Street_id':street_id,
                                   'Point_id':point_id,
                                   'Dzielnice' : dzielnica, 
                                   'Nazwa_skrocona' : nazwa_skrocona, 
                                   'Nazwa_peln': nazwa_peln}, ignore_index = True)
                point_id += 1
            street_id += 1
    
#res_df.describe()

res_df.drop('Dzielnica', axis = 1, inplace = True)
res_df['Street_id_un'] = res_df.Street_id+10000
res_df['Dzielnica'] = res_df['Dzielnice'].apply(lambda x: x.split(',')[0])


res_df.to_csv('/Users/glebsmolnik/Documents/Streets_All.csv',index = False, encoding='utf-8')


res_df.head()
res_df[res_df.Nazwa_skrocona == "ul. K. Deyny"]

l = []

for feature in data['features']:
    l.append(feature['geometry']['type'])
    
un = res_df.loc[:,["Street_id","Nazwa_peln"]].drop_duplicates()
res_df.unique()
res_df.groupby('Street_id').Nazwa_peln.nunique().sort_values()
