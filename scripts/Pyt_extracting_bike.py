import urllib.request, json 
import pandas as pd
with urllib.request.urlopen("http://mapa.um.warszawa.pl/WebServices/TrasyRowerowe/wgs84/findAll/") as url:
    data = json.loads(url.read().decode())
    


"""
Latitude = 52
Long = 21
"""

bikes = pd.DataFrame(columns=['Long','Lat','Route_Id','Point_id','Dzielnica', 'Lokalizacja', 'Typ_nawierzchni', 'Typ_trasy'])

#data['features'][1]['geometry']['coordinates'][0][0][0]

"""

dzielnica
lokalizacja
typ_nawierzchni
typ_trasy

"""

route_id = 1

for feature in data['features']:
    dzielnica = feature['properties']['dzielnica']
    lokalizacja = feature['properties']['lokalizacja']
    typ_nawierzchni = feature['properties']['typ_nawierzchni']
    typ_trasy = feature['properties']['typ_trasy']
    if feature['geometry']['type'] == 'LineString':
        point_id = 1
        for point in feature['geometry']['coordinates']:
            #print(point)
            bikes = bikes.append({'Long' : point[0], 
                                   'Lat': point[1], 
                                   'Route_Id':route_id,
                                   'Point_id':point_id,
                                   'Dzielnica' : dzielnica, 
                                   'Lokalizacja' : lokalizacja, 
                                   'Typ_nawierzchni': typ_nawierzchni,
                                   'Typ_trasy':typ_trasy}, ignore_index = True)
            point_id += 1
        route_id += 1
    else:
        for route in feature['geometry']['coordinates']:
            point_id = 1
            for point in route:
                #print(point)
                bikes = bikes.append({'Long' : point[0], 
                                   'Lat': point[1], 
                                   'Route_Id':route_id,
                                   'Point_id':point_id,
                                   'Dzielnica' : dzielnica, 
                                   'Lokalizacja' : lokalizacja, 
                                   'Typ_nawierzchni': typ_nawierzchni,
                                   'Typ_trasy':typ_trasy}, ignore_index = True)
                point_id += 1
            route_id += 1
    
#bikes.describe()

bikes.to_csv('/Users/glebsmolnik/Documents/Routes_All.csv',index = False,encoding='utf-8')

bikes.head()

l = []

for feature in data['features']:
    l.append(feature['geometry']['type'])
    
