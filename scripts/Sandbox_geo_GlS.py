#print(warsaw_borders[warsaw_borders['Dzielnica']=='Mokotów'].Long.max(),
#warsaw_borders[warsaw_borders['Dzielnica']=='Mokotów'].Long.min(),
#warsaw_borders[warsaw_borders['Dzielnica']=='Mokotów'].Lat.max(),
#warsaw_borders[warsaw_borders['Dzielnica']=='Mokotów'].Lat.min())
import numpy as np

from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
 

lons_lats_vect = np.array(data['features'][2]['geometry']['coordinates'][0])

polygon = Polygon(lons_lats_vect) # create polygon
point = Point(20.9978034,52.1991156) # create point
print(point.within(polygon)) # check if polygon contains point

i = 0
for feature in data['features']:
    print(feature['properties']['nazwa_dzielnicy'], i )
    i += 1
        
from shapely.geometry import box, MultiPolygon

def fishnet(geometry, threshold):
    bounds = geometry.bounds
    xmin = int(bounds[0] // threshold)
    xmax = int(bounds[2] // threshold)
    ymin = int(bounds[1] // threshold)
    ymax = int(bounds[3] // threshold)
    ncols = int(xmax - xmin + 1)
    nrows = int(ymax - ymin + 1)
    result = []
    for i in range(xmin, xmax+1):
        for j in range(ymin, ymax+1):
            b = box(i*threshold, j*threshold, (i+1)*threshold, (j+1)*threshold)
            g = geometry.intersection(b)
            if g.is_empty:
                continue
            result.append(g)
    return result  

obj = fishnet(polygon, 0.01)
m = MultiPolygon(obj)
m


from shapely.geometry import box, Polygon, MultiPolygon, GeometryCollection

def katana(geometry, threshold, count=0):
    """Split a Polygon into two parts across it's shortest dimension"""
    bounds = geometry.bounds
    width = bounds[2] - bounds[0]
    height = bounds[3] - bounds[1]
    if max(width, height) <= threshold or count == 250:
        # either the polygon is smaller than the threshold, or the maximum
        # number of recursions has been reached
        return [geometry]
    if height >= width:
        # split left to right
        a = box(bounds[0], bounds[1], bounds[2], bounds[1]+height/2)
        b = box(bounds[0], bounds[1]+height/2, bounds[2], bounds[3])
    else:
        # split top to bottom
        a = box(bounds[0], bounds[1], bounds[0]+width/2, bounds[3])
        b = box(bounds[0]+width/2, bounds[1], bounds[2], bounds[3])
    result = []
    for d in (a, b,):
        c = geometry.intersection(d)
        if not isinstance(c, GeometryCollection):
            c = [c]
        for e in c:
            if isinstance(e, (Polygon, MultiPolygon)):
                result.extend(katana(e, threshold, count+1))
    if count > 0:
        return result
    # convert multipart into singlepart
    final_result = []
    for g in result:
        if isinstance(g, MultiPolygon):
            final_result.extend(g)
        else:
            final_result.append(g)
    return final_result

obj1 = katana(polygon, 0.01)
m1 = MultiPolygon(obj1)
m1

list(zip(*obj1[0].exterior.centroid.coords.xy))
list(zip(*obj1[0].exterior.coords.xy))


with open('/Users/glebsmolnik/Documents/MAD/data/warsaw.geojson', 'r') as f:
    w_data = json.load(f)
    
w_map = np.array(w_data['geometry']['coordinates'][0][0])
pol_wars = Polygon(w_map)


wars_ds = pd.DataFrame(columns=['Long','Lat'])
for point in w_data['geometry']['coordinates'][0][0]:
    wars_ds = wars_ds.append({'Long' : point[0],
                              'Lat' : point[1]}, ignore_index = True)
wars_ds.to_csv('/Users/glebsmolnik/Documents/MAD/data/Warsaw_Borders.csv',index = False, encoding='utf-8')

wars_obj1 = fishnet(pol_wars, 0.01)

len(wars_obj1)
MultiPolygon(wars_obj1)
pere = Polygon(wars_obj1[120])
pere.area*6370**2
pol_wars.area*6370**2
--517,24



import pyproj
from shapely.geometry import shape
from shapely.ops import transform
from functools import partial

proj = partial(pyproj.transform, pyproj.Proj(init='epsg:4326'),
               pyproj.Proj(init='epsg:3857'))

s_new = transform(proj, polygon)
projected_area = transform(proj, polygon).area
projected_area/1000/1000

wars_obj1[120].bounds

projected_area = transform(proj, s).area