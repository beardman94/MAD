import json, requests, sys, time
from shapely.geometry import asShape, mapping


def form_request(lon, lat, radius=941):
    return "http://api.locit.pl/webservice/catchment-data/v2.0.0/{}/{}/{}?format=json&charset=UTF-8".format(radius, lat, lon)


def get_request(lon, lat, radius):
    return requests.get(form_request(lon, lat, radius),
                        headers={'Authorization': 'maraton0n895gbsgc72bbksa042mad17', 'accept': "application/json"}).json()['data']


if __name__ == "__main__":
    input_file = sys.argv[1]
    output_file = sys.argv[2]

    with open(input_file, 'r') as file:
        polygz = json.load(file)

    lines = {}
    for feature in polygz['features']:
        lines[feature['properties']['id']] = asShape(feature['geometry'])

    geojson = {
        "type": "FeatureCollection",
        "features": []
    }

    for id, poly in lines.items():
        centroid = poly.centroid.xy
        lon = centroid[0][0]
        lat = centroid[1][0]

        data = get_request(lon, lat, radius=941)
        data['id'] = id

        geojson['features'].append({
            "type": "Feature",
            "properties": data,
            "geometry": mapping(poly)
        })
        print("[INFO] GET DATA FOR {}".format(id))
        time.sleep(1)
    json.dump(geojson, open(output_file, 'w'))
    print("FINISHED")





