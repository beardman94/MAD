import urllib.request, json 
import pandas as pd
with urllib.request.urlopen("http://mapa.um.warszawa.pl/WebServices/GraniceDzielnic/wgs84/findAll/") as url:
    data = json.loads(url.read().decode())
    


"""
Latitude = 52
Long = 21
"""

warsaw_borders = pd.DataFrame(columns=['Long','Lat','Dzielnica','Point_id','Border_id'])

#data['features'][1]['geometry']['coordinates'][0][0][0]

"""

dzielnica
lokalizacja
typ_nawierzchni
typ_trasy

"""
border_id = 1

for feature in data['features']:
    nazwa_dzielnicy = feature['properties']['nazwa_dzielnicy']
    #typ_trasy = feature['properties']['typ_trasy']
    if feature['geometry']['type'] == 'LineString':
        point_id = 1
        for point in feature['geometry']['coordinates']:
            #print(point)
            warsaw_borders = warsaw_borders.append({'Long' : point[0], 
                                   'Lat': point[1], 
                                   'Border_id':border_id,
                                   'Point_id':point_id,
                                   'Dzielnica' : nazwa_dzielnicy}, ignore_index = True)
            point_id += 1
        border_id += 1
    elif feature['geometry']['type'] != 'MultiPolygon':
        for street in feature['geometry']['coordinates']:
            point_id = 1
            for point in street:
                #print(point)
                warsaw_borders = warsaw_borders.append({'Long' : point[0], 
                                   'Lat': point[1], 
                                   'Border_id':border_id,
                                   'Point_id':point_id,
                                   'Dzielnica' : nazwa_dzielnicy}, ignore_index = True)
                point_id += 1
            border_id += 1
    
#res_df.describe()

warsaw_borders.drop('Dzielnica', axis = 1, inplace = True)
warsaw_borders['Street_id_un'] = warsaw_borders.Street_id+10000
warsaw_borders['Dzielnica'] = warsaw_borders['Dzielnice'].apply(lambda x: x.split(',')[0])


warsaw_borders.to_csv('/Users/glebsmolnik/Documents/MAD/data/District_Borders_All.csv',index = False, encoding='utf-8')


warsaw_borders.head()
warsaw_borders[warsaw_borders.Nazwa_skrocona == "ul. K. Deyny"]

l = []

for feature in data['features']:
    l.append(feature['geometry']['type'])
    
un = warsaw_borders.loc[:,["Street_id","Nazwa_peln"]].drop_duplicates()
warsaw_borders.unique()
warsaw_borders.groupby('Street_id').Nazwa_peln.nunique().sort_values()


'http://mapa.um.warszawa.pl/WebServices/GraniceDzielnic/wgs84/findAll/'