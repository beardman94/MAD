import json
import sys
from shapely.geometry import box, Polygon, mapping

# example of using:
# python3.5 scripts/split_2_squares.py data/warsaw.geojson data/warsaw_splitted.json 0.015


def fishnet(geometry, threshold):
    bounds = geometry.bounds
    xmin = int(bounds[0] // threshold)
    xmax = int(bounds[2] // threshold)
    ymin = int(bounds[1] // threshold)
    ymax = int(bounds[3] // threshold)
    ncols = int(xmax - xmin + 1)
    nrows = int(ymax - ymin + 1)
    result = []
    for i in range(xmin, xmax+1):
        for j in range(ymin, ymax+1):
            b = box(i*threshold, j*threshold, (i+1)*threshold, (j+1)*threshold)
            g = geometry.intersection(b)
            if g.is_empty:
                continue
            result.append(g)
    return result


if __name__ == "__main__":
    input_filename = sys.argv[1]
    output_filename = sys.argv[2]
    granularity = sys.argv[3]

    with open(input_filename, 'r') as file:
        input_f = json.load(file)

    coord_input_file = input_f['geometry']['coordinates'][0][0]

    coord_poly = Polygon(coord_input_file)
    splitted_poly = fishnet(coord_poly, float(granularity))

    geojson = {
        "type": "FeatureCollection",
        "features": []
    }

    for id, poly in enumerate(splitted_poly):
        geojson['features'].append({
            "type": "Feature",
            "properties": {"id": id},
            "geometry": mapping(poly)
        })

    json.dump(geojson, open(output_filename, 'w'))
    print("[SUCCESS!!! YEAH, BITCHES !!! ]")
